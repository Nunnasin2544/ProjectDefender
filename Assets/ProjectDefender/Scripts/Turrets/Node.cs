using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
   public Color hoverColor;
   public Color errorColor;
   public Vector3 positionOffset;

   [HideInInspector]
   public GameObject turret;
   [HideInInspector]
   public TurretBP turretBP;
   [HideInInspector]
   public bool isUpgraded = false;
   
   public GameObject destroyEffect;

   private Renderer rend;
   private Color startColor;
   
   BuildManager buildManager;

   private void Start()
   {
      rend = GetComponent<Renderer>();
      startColor = rend.material.color;
      
      buildManager = BuildManager.instance;
   }   

   public Vector3 GetBuildPosition()
   {
      return transform.position + positionOffset;
   }

   private void OnMouseDown()
   {
      if (EventSystem.current.IsPointerOverGameObject())
          return;

      if (turret != null)
      {
         buildManager.SelectNode(this);
         return;
      }
      
      if (!buildManager.CanBuild)
          return;
      
      BuildTurret(buildManager.GetTurretToBuild());
      OnMouseExit();
   }
   
   void BuildTurret(TurretBP blueprint)
   {
       if (BuildManager.CurrentTurret == 0)
       {
           Debug.Log ("Limited turret!");
           return;
       }
    
       if (PlayerStats.Money < blueprint.cost)
       {
           Debug.Log ("Not enough money to build that!");
           return;
       }
        
       PlayerStats.Money -= blueprint.cost;
       BuildManager.CurrentTurret -= 1;
        
       GameObject _turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.identity);
       turret = _turret;
       
       turretBP = blueprint;

       var audiosource = GetComponent<AudioSource>();
       audiosource.Play();
       GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
       Destroy(effect, 5f);
        
       Debug.Log ("Turret Builded!");
   }
   
   public void UpgradeTurret()
   {
       if (PlayerStats.Money < turretBP.upgradeCost)
       {
          Debug.Log ("Not enough money to upgrade that!");
          return;
       }
               
       PlayerStats.Money -= turretBP.upgradeCost;
       
       Destroy(turret);
               
       GameObject _turret = (GameObject)Instantiate(turretBP.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
       turret = _turret;
       
       GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
       Destroy(effect, 5f);
       
       isUpgraded = true;
               
       Debug.Log ("Turret Upgraded!");
   }
   
   public void SellTurret()
   {
      if (isUpgraded == true)
      {
          PlayerStats.Money += turretBP.GetSellAmount2();
          isUpgraded = false;
      }
      
      else if (isUpgraded == false)
      {
            PlayerStats.Money += turretBP.GetSellAmount();
      }
            
      BuildManager.CurrentTurret += 1;
          
      Destroy(turret);
      GameObject effect = (GameObject)Instantiate(destroyEffect, GetBuildPosition(), Quaternion.identity);
      Destroy(effect, 5f);
      turretBP = null;
    }

   void OnMouseEnter ()
   {
      if (EventSystem.current.IsPointerOverGameObject())
          return;
   
      if (!buildManager.CanBuild)
          return;
          
      if (!buildManager.HasMoney || buildManager.LimitBuild)
      {
          rend.material.color = errorColor; 
      }
      else
      {
          rend.material.color = hoverColor;
      }
   }

   private void OnMouseExit()
   {
      rend.material.color = startColor;
   }
}
