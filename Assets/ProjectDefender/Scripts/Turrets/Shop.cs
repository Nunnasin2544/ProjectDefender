using System;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [Header("Set Cost")]
    public TurretBP GunTurret;
    public TurretBP LaserTurret;
    public TurretBP GummyTurret;
    public TurretBP AirTurret;
    
    [Header("Turret Selected")]
    public Color defaultColor;
    public Color imageColor;
    public Image gunIcon;
    public Image laserIcon;
    public Image gummyIcon;
    public Image airIcon;

    BuildManager buildManager;

    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    
    void Update()
    {
            if (Input.GetKeyDown("c"))
            {
                 buildManager.SelectTurretToBuild(null);
                 gunIcon.color = defaultColor;
                 laserIcon.color = defaultColor;
                 gummyIcon.color = defaultColor;
                 airIcon.color = defaultColor;
                 CameraController.lockCursor = true;
                 Debug.Log("Canceled Build!");
             }
             if (Input.GetKeyDown("1"))
             {
                 SelectGunTurret();
                 gunIcon.color = imageColor;
                 laserIcon.color = defaultColor;
                 gummyIcon.color = defaultColor;
                 airIcon.color = defaultColor;
                 var audiosource = GetComponent<AudioSource>();
                 audiosource.Play();
                 CameraController.lockCursor = false;
                 
             }
             if (Input.GetKeyDown("2"))
             {
                 SelectLaserTurret();
                 gunIcon.color = defaultColor;
                 laserIcon.color = imageColor;
                 gummyIcon.color = defaultColor;
                 airIcon.color = defaultColor;
                 var audiosource = GetComponent<AudioSource>();
                 audiosource.Play();
                 CameraController.lockCursor = false;
             }
             if (Input.GetKeyDown("3"))
             {
                 SelectGummyTurret();
                 gunIcon.color = defaultColor;
                 laserIcon.color = defaultColor;
                 gummyIcon.color = imageColor;
                 airIcon.color = defaultColor;
                 var audiosource = GetComponent<AudioSource>();
                 audiosource.Play();
                 CameraController.lockCursor = false;
             }
             if (Input.GetKeyDown("4"))
             {
                 SelectAirTurret();
                 gunIcon.color = defaultColor;
                 laserIcon.color = defaultColor;
                 gummyIcon.color = defaultColor;
                 airIcon.color = imageColor;
                 var audiosource = GetComponent<AudioSource>();
                 audiosource.Play();
                 CameraController.lockCursor = false;
             }
    }

    public void SelectGunTurret()
    {
        Debug.Log("Gun Turret Selected");
        buildManager.SelectTurretToBuild(GunTurret);
    }
    
    public void SelectLaserTurret()
    {
        Debug.Log("Laser Turret Selected");
        buildManager.SelectTurretToBuild(LaserTurret);
    }
    
    public void SelectGummyTurret()
    {
        Debug.Log("Gummy Turret Selected");
        buildManager.SelectTurretToBuild(GummyTurret);
    }
    
    public void SelectAirTurret()
    {
        Debug.Log("Air Turret Selected");
        buildManager.SelectTurretToBuild(AirTurret);
    }
}
