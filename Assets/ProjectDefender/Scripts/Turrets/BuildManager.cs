using UnityEngine;
using System.Collections;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;
    
    public int limitTurret = 12;
    public static int LimitTurret;
    public static int CurrentTurret;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one BuildManager is scene!");
            return;
        }
        instance = this;
    }
    
    void Start()
    {
        LimitTurret = limitTurret;
        CurrentTurret = LimitTurret;
    }

    /*public GameObject GunTurretPrefab;
    public GameObject GummyTurretPrefab;
    public GameObject AirTurretPrefab;*/
    
    public GameObject buildEffect;
    
    private TurretBP turretToBuild;
    private Node SelectedNode;

    public NodeUI nodeUI;
    
    public bool CanBuild { get {return turretToBuild != null; } }
    public bool HasMoney { get {return PlayerStats.Money >= turretToBuild.cost; } }
    public bool LimitBuild { get {return CurrentTurret == 0; } }
    
    public void SelectNode(Node node)
    {
        if (SelectedNode == node)
        {
            DeSelectNode();
            return;
        }
        
        SelectedNode = node;
        turretToBuild = null;

        nodeUI.SetTarget(node);
    }

    public void DeSelectNode()
    {
        SelectedNode = null;
        nodeUI.Hide();
    }

    public void SelectTurretToBuild(TurretBP turret)
    {
        turretToBuild = turret;
        DeSelectNode();
    }

    public TurretBP GetTurretToBuild()
    {
        return turretToBuild;
    }
}
