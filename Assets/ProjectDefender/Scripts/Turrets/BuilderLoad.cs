using System;
using UnityEngine;
using UnityEngine.UI;

 public class BuilderLoad : MonoBehaviour
 {
 [SerializeField] protected float m_TimerDuration = 5;

 [SerializeField] protected Slider m_SliderTimer;
 private bool _IsTimerStart = false;
 private bool _IsBuildFirstTIme = true;
 private float _StartTimeStamp;
 private float _EndTimeStamp;
 private float _SliderValue;

void Update()
 {
    if (!_IsTimerStart && !_IsBuildFirstTIme) return;
    
    if (WaveSpawner.EnemiesAlive > 0)
    {
        BuildFirstTime();
        _SliderValue = ((Time.time - _StartTimeStamp)/ m_TimerDuration)*m_SliderTimer.maxValue;
        m_SliderTimer.value = _SliderValue;
    
        if (Time.time >= _EndTimeStamp)
        {
           _IsTimerStart = false;
        }
    }
    
    else if (WaveSpawner.EnemiesAlive == 0)
    {
        var audiosource = GetComponent<AudioSource>();
        audiosource.Stop();
        _IsBuildFirstTIme = false;
        _IsTimerStart = false;
    }
  }

 public void BuildFirstTime()
 {
     if(_IsBuildFirstTIme == true)
     {
     var audiosource = GetComponent<AudioSource>();
     audiosource.Play();
     StartBuildTimer();
     Debug.Log("Load!");
     }
 }

 public void StartBuildTimer()
 {
     //Check if the timer is already running
 if (_IsTimerStart) return;
    
    _IsTimerStart = true;
    _StartTimeStamp = Time.time;
    _EndTimeStamp = Time.time + m_TimerDuration;
    _SliderValue = 0;
    _IsBuildFirstTIme = false;
 }

}