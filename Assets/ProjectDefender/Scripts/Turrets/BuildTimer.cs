using System;
using UnityEngine;
using UnityEngine.Events;

public class BuildTimer : MonoBehaviour
 {
     [SerializeField] protected float m_TimerDuration = 5;

 [SerializeField] protected UnityEvent m_TimerStartEvent = new();
 [SerializeField] protected UnityEvent m_TimerEndEvent = new();

 protected bool _IsTimerStart = false;
 protected bool _IsBuildFirstTIme = true;
 protected float _StartTimeStamp;
 protected float _EndTimeStamp;
[SerializeField] protected float _CurrentTime;

 void Update()
 {
     if (!_IsTimerStart && !_IsBuildFirstTIme) return;
     
     if (WaveSpawner.EnemiesAlive > 0 || !WaveSpawner.endAllWave|| WaveSpawner.endSpawner)
     {
     BuildFirstTime();
     
             _CurrentTime = (Time.time - _StartTimeStamp);
             if (Time.time >=_EndTimeStamp)
             {
                  EndBuildTimer();
             }
     }
     
     else if (WaveSpawner.EnemiesAlive == 0 && WaveSpawner.endAllWave)
          {
              _IsBuildFirstTIme = false;
              EndBuildTimer();
          }
 
    /*if (!_IsTimerStart && WaveSpawner.EnemiesAlive == 0) 
    {
        BuildFirstTime();
        
        _CurrentTime = (Time.time - _StartTimeStamp);
        if (Time.time >=_EndTimeStamp)
        {
             EndBuildTimer();
        }
    }*/
    
     /*else if (!_IsTimerStart && WaveSpawner.EnemiesAlive == 0)
     {
         _IsBuildFirstTIme = false;
         _IsTimerStart = true;
     }*/
 }

public void BuildFirstTime()
{
    if(_IsBuildFirstTIme == true)
    {
    StartBuildTimer();
    }
}

 public virtual void StartBuildTimer()
 {
     //Check if the timer is already running
     if (_IsTimerStart) return;
    
     m_TimerStartEvent.Invoke();
    
    _IsTimerStart = true;
    _StartTimeStamp = Time.time; 
    _EndTimeStamp = Time.time + m_TimerDuration;
    _IsBuildFirstTIme = false;
    }

 public virtual void EndBuildTimer(){
     m_TimerEndEvent.Invoke();
    _IsTimerStart = false;
     }
 }
 