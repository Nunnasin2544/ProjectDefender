using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretBP
{
   public GameObject prefab;
   public int cost;
   
   public GameObject upgradedPrefab;
   public int upgradeCost;
   
   public int GetSellAmount()
   {
      return cost/2;
   }
   
   public int GetSellAmount2()
   {
      return (cost+upgradeCost)/2;
   }
}
