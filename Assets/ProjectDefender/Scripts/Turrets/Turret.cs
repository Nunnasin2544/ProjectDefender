using System.Collections;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private Transform target;
    private Enemy targetEnemy;
    
    [Header("General")]
    public float range = 15f;
    
    [Header("Use Bullets (default)")]
    public GameObject bulletPrefab;
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    
    [Header("Use Laser")]
    public bool useLaser = false;
    public int damageOverTime = 30;
    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;

    [Header("Unity Setup Fields")]
    
    public string enemyTag = "Enemy";

    public Transform partToRotate;
    public  float turnSpeed = 10f;
    
    public Transform firePoint;

    void Start ()
    {    
        InvokeRepeating ("UpdateTarget", 0f, 0.5f); 
    }
    
    void UpdateTarget ()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            target = null;
        }
    }
    
    void Update ()
    {
        if (target == null)
        {
            if(useLaser)
            {
                if(lineRenderer.enabled)
                {
                   lineRenderer.enabled = false;
                   impactEffect.Stop();
                   var audiosource = GetComponent<AudioSource>();
                   audiosource.Stop();
                   impactLight.enabled = false;
                }
            }
            
            return;
        }
            
         LockOnTarget();
         
         if(useLaser)
         {
             LaserShoot();
         }
         else
         {
             if (fireCountdown <= 0f)
             {
                 Shoot();
                 fireCountdown = 1f / fireRate;
             }
                                 
              fireCountdown -= Time.deltaTime;
         }                   
    }
    
    void LockOnTarget()
    {
        Vector3 trg = target.position;
        trg = new Vector3(trg.x, trg.y+1.5f, trg.z);
    
        Vector3 dir = trg - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(rotation.x, rotation.y, 0f);
    }
    
    void LaserShoot()
    {
        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
    
        if(!lineRenderer.enabled)
        {
           lineRenderer.enabled = true;
           impactEffect.Play();
           var audiosource = GetComponent<AudioSource>();
           audiosource.Play();
           impactLight.enabled = true;
        }
    
        Vector3 trg = target.position;
        trg = new Vector3(trg.x, trg.y+1.5f, trg.z);
        
        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, trg);
        
        Vector3 dir = firePoint.position - trg;
        
        impactEffect.transform.position = trg + dir.normalized * .5f;
        
        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
    }

    void Shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        AirMissile missile = bulletGO.GetComponent<AirMissile>();
        
        if (bullet != null)
            bullet.Seek(target);
            
        if (missile != null)
            missile.Seek(target);
            
        var audiosource = GetComponent<AudioSource>();
        audiosource.Play();
    }    

    void OnDrawGizmosSelected ()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
