using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Explosion : MonoBehaviour
{
    [HideInInspector]
    public float skillRadius = 100f;
    public GameObject impactEffect;
    
    [Header("Set Mode")]
    public bool SuperMode = false;
    
    [Header("Cannon Skill")]
    public bool cannonUsed = true;
    public float cannonStartCooldown = 20f;
    private float cannonCooldown;
    public float cannonPower = 100f;
    
    [Header("Unity Stuff")]
    public Image Cannon;
    
    void Start()
    {
       cannonCooldown = cannonStartCooldown;
    }

    void Update()
    {
        Cannon.fillAmount = cannonCooldown / cannonStartCooldown;
    
       if (SuperMode)
       {
        if (!cannonUsed)
        {
           if(Input.GetKeyDown("q"))
           {
               CannonShoot();
               var audiosource = GetComponent<AudioSource>();
               audiosource.Play();
               GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
               Destroy(effectIns, 2f);
               cannonUsed = true;
               cannonCooldown = cannonStartCooldown;
               Debug.Log("Cannon Used!");
           }
           
        }
        else
        {
            if(Input.GetKeyDown("q"))
            {  
                Debug.Log("Cannon is cooldown!");
            }
            
            if (cannonCooldown <= 0f)
            {
                cannonUsed = false;
            }
        
            cannonCooldown -= Time.deltaTime;
        }
       }  
    }
    
    void CannonShoot()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, skillRadius);
        foreach (Collider collider in colliders)
        {
             if (collider.tag == "Enemy")
             {
                  CannonDamage(collider.transform);
             }
         }
    }
    
    void CannonDamage (Transform enemy)
        {
            Enemy e = enemy.GetComponent<Enemy>();
            
            if (e != null)
            {
                e.TakeDamage(cannonPower);
            } 
        }    
}
