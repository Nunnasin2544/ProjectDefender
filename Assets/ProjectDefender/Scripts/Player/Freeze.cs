using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Freeze : MonoBehaviour
{
    [HideInInspector]
    public float skillRadius = 100f;
    public GameObject impactEffect;
    
    [Header("Freeze Skill")]
    public bool freezeUsed = true;
    public float freezeStartCooldown = 20f;
    private float freezeCooldown;
    public float freezePower = 1f;
    
    [Header("Unity Stuff")]
    public Image freeze;
    
    void Start()
    {
       freezeCooldown = freezeStartCooldown;
    }

    void Update()
    {
        freeze.fillAmount = freezeCooldown / freezeStartCooldown;
        
         if (!freezeUsed)
                {
                   if(Input.GetKeyDown("e"))
                   {
                       FreezeShoot();
                       var audiosource = GetComponent<AudioSource>();
                       audiosource.Play();
                       GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
                       Destroy(effectIns, 2f);
                       freezeUsed = true;
                       freezeCooldown = freezeStartCooldown;
                       Debug.Log("Freeze Used!");
                   }
                   
                }
                else
                {
                    if(Input.GetKeyDown("e"))
                    {  
                        Debug.Log("Freeze is cooldown!");
                    }
                    
                    if (freezeCooldown <= 0f)
                    {
                        freezeUsed = false;
                    }
                
                    freezeCooldown -= Time.deltaTime;
                }
    }
    
     void FreezeShoot()
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, skillRadius);
            foreach (Collider collider in colliders)
            {
                 if (collider.tag == "Enemy")
                 {
                      FreezeTime(collider.transform);
                 }
             }
        }
        
        void FreezeTime (Transform enemy)
            {
                Enemy e = enemy.GetComponent<Enemy>();
                
                if (e != null)
                {
                    e.Freeze(freezePower);
                }
            }    
}
