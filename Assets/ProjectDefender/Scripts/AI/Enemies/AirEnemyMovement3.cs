using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AirEnemy))]
public class AirEnemyMovement3 : MonoBehaviour
{
    private float RotX = -77.155f;
    private float RotZ = -83.156f;

    private Transform target;
    private int wavepointIndex = 0;

    private AirEnemy airenemy;

    void Start()
    {
        airenemy = GetComponent<AirEnemy>();
        
        target = AirWaypoints3.points[0];
    }
    
    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * airenemy.speed * Time.deltaTime, Space.World);
        
        Vector3 rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * airenemy.speed).eulerAngles;
        transform.rotation = Quaternion.Euler(RotX, rotation.y, RotZ);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= AirWaypoints3.points.Length - 1)
        {
            EndPath();
            return;
        }

        wavepointIndex++;
        target = AirWaypoints3.points[wavepointIndex];
    }
    
    void EndPath()
    {
        PlayerStats.Lives -= (int)Mathf.Round(airenemy.health);
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
