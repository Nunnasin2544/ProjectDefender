using System;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float startSpeed = 10f;
    
    public float startHealth = 100;
    [HideInInspector]
    public float health;
    private bool isDead = false;
    
    public int moneyDrop = 10;
    
    [HideInInspector]
    public float speed;
    [HideInInspector]
    public bool isFreeze = false;
    public float slowCooldown;
    
    [Header("Set Boss")]
    public bool isBoss = false;
    
    [Header("Set Slow Time")]
    public float slowTime = 1f;
    public float freezeTime = 3f;

    [Header("Unity Stuff")]
    public Image healthBar;

    void Start()
    {
        speed = startSpeed;
        health = startHealth;
    }
    
    public void TakeDamage (float amount)
    {
        health -= amount;
        
        healthBar.fillAmount = health / startHealth;
        
        if (health <= 0)
        {
            Die();
        }    
    }

    public void Slow(float pct)
    {
        if(isFreeze == false)
        {
           speed = startSpeed * (1f - pct);
            slowCooldown = slowTime;
        }
    }
    
    public void Freeze(float pct)
    {
        speed = startSpeed * (1f - pct);
        slowCooldown = freezeTime;
        isFreeze = true;
    }
    
    void Die()
    {
        if(!isDead)
        {
            PlayerStats.Money += moneyDrop;
            WaveSpawner.EnemiesAlive--;
            isDead = true;
        }
        Destroy(gameObject);
    }
}
