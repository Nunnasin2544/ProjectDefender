using System;
using UnityEngine;
using UnityEngine.UI;

public class AirEnemy : MonoBehaviour
{
    public float speed = 10f;
    
    public float startHealth = 100;
    [HideInInspector]
    public float health;
    
    public int moneyDrop = 10;
    
    [Header("Unity Stuff")]
    public Image healthBar;

    void Start()
    {
        health = startHealth;
    }
    
    public void TakeDamage (int amount)
    {
        health -= amount;
        
        healthBar.fillAmount = health / startHealth;
        
        if (health <= 0)
        {
            Die();
        }    
    }
    
    void Die()
    {
        PlayerStats.Money += moneyDrop;
        
        WaveSpawner.EnemiesAlive--;
        
        Destroy(gameObject);
    }
}
