using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMovement2 : MonoBehaviour
{
    private Transform target;
    private int wavepointIndex = 0;

    private Enemy enemy;

    void Start()
    {
        enemy = GetComponent<Enemy>();
        
        target = Waypoints2.points[0];
    }
    
    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);
        
        Vector3 rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * enemy.speed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
        
        if (enemy.slowCooldown <= 0f)
        {
            enemy.speed = enemy.startSpeed;
            enemy.slowCooldown = 0f;
            enemy.isFreeze = false;
        }
        else
        {
            enemy.slowCooldown -= Time.deltaTime;
        }
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints2.points.Length - 1)
        {
            EndPath();
            return;
        }

        wavepointIndex++;
        target = Waypoints2.points[wavepointIndex];
    }
    
    void EndPath()
    {
        PlayerStats.Lives -= (int)Mathf.Round(enemy.health);
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
