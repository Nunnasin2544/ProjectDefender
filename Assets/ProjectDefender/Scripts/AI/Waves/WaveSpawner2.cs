using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;

public class WaveSpawner2 : MonoBehaviour
{
    //public static int EnemiesAlive = 0;
    public int Alive;
    
    public Wave[] waves;
    
    public Transform spawnPoint;
    
    public float timeBetweenWaves = 5f;
    public float countdown = 10f;
    public static bool endWave = true;
    private bool endAllWave = true;
    public static bool endSpawner = false;

    private int waveIndex = 0;
    public static int waveCount = 1;

    void Update ()
    {
       Alive = WaveSpawner.EnemiesAlive;
       
       if(endSpawner && endAllWave)
       {
           this.enabled = false;
       }
       
       if(WaveSpawner.endWave && WaveSpawner3.endWave && endWave)
       {
           if(WaveSpawner.EnemiesAlive == 0)
           {
               endAllWave = true;
           }
       }    
       
       if(!endAllWave)
       {
           return;
       }
       
       /*if (EnemiesAlive != 0)
       {
           return;
       }*/
       
       if (!endWave)
       {
           return;
       }
       
    
       if (countdown <= 0f && endAllWave)
       {
          StartCoroutine(SpawnWave());
          countdown = timeBetweenWaves;
          endAllWave = false;
          endWave = false;
          return;
       }

           waveCount = waveIndex;
           waveCount += 1;
           countdown -= Time.deltaTime;
       
       if (Input.GetKeyDown("r") && countdown > 1f)
       {
           countdown = 1f;
       }

    }
    
    IEnumerator SpawnWave()
    {
      if(!endSpawner)
      {
        Wave wave = waves[waveIndex];
    
        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        endWave = true;
        waveIndex++;
        
        if (waveIndex == waves.Length)
        {
            endSpawner = true;
        }
      }
    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        WaveSpawner.EnemiesAlive++;
    }
}
