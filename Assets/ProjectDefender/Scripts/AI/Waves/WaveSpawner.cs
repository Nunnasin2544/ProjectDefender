using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WaveSpawner : MonoBehaviour
{
    public static int EnemiesAlive = 0;
    public int Alive;
    
    public Wave[] waves;
    
    public Transform spawnPoint;
    
    public float timeBetweenWaves = 5f;
    public float countdown = 10f;
    public static bool endWave = true;
    public static bool endAllWave = true;
    public static bool endSpawner = false;
    
    public TextMeshProUGUI waveCountdownText;
    
    private int waveIndex = 0;
    public static int waveCount = 1;
    
    [Header("Setup Waves")]
    public Transform Spawner2;
    public int onWave2 = 1;
    public Transform Spawner3;
    public int onWave3 = 1;

    void Update ()
    {
       Alive = EnemiesAlive;
       
       if(endSpawner && endAllWave && EnemiesAlive == 0 && waveIndex == 10)
       {
           SceneManager.LoadScene("GameClear");
           return;
       }
       
       if(WaveSpawner2.endWave && WaveSpawner3.endWave && endWave)
       {
           if(EnemiesAlive == 0)
           {
               endAllWave = true;
           }
       }
              
       if(!endAllWave)
       {
           return;
       }
       
       /*if (EnemiesAlive != 0)
       {
           return;
       }*/
       
       if (!endWave)
       {
           return;
       }
       
    
       if (countdown <= 0f && endAllWave)
       {
          StartCoroutine(SpawnWave());
          countdown = timeBetweenWaves;
          endAllWave = false;
          endWave = false;
          return;
       }

           waveCount = waveIndex;
           if(!endSpawner)
              waveCount += 1;
           countdown -= Time.deltaTime;
       
       if (Input.GetKeyDown("r") && countdown > 1f)
       {
           countdown = 1f;
           Debug.Log("Skip Phase!");
       }

       if(countdown == timeBetweenWaves || endSpawner)
       {
           waveCountdownText.text = "0";
       }
       else
           waveCountdownText.text = Mathf.Round(countdown).ToString();
    }
    
    IEnumerator SpawnWave()
    {
        if(!endSpawner)
        {
          Wave wave = waves[waveIndex];
    
          for (int i = 0; i < wave.count; i++)
          {
              SpawnEnemy(wave.enemy);
              yield return new WaitForSeconds(1f / wave.rate);
          }

          endWave = true;
          waveIndex++;
        
          if (waveIndex == onWave2)
          {
              WaveSpawner2 sp2 = Spawner2.GetComponent<WaveSpawner2>();
              sp2.enabled = true;
          }
          
          if (waveIndex == onWave3)
          {
              WaveSpawner3 sp3 = Spawner3.GetComponent<WaveSpawner3>();
              sp3.enabled = true;
          }
        
          if (waveIndex == waves.Length)
          {
              Debug.Log("WAVE FINALE!");
              endSpawner = true;
          }
        }
    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        EnemiesAlive++;
    }
}
