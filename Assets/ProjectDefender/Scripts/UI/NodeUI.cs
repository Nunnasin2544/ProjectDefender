using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NodeUI : MonoBehaviour
{
    public GameObject ui;
    
    [Header("Set Mode")]
    public bool SuperMode = false;
    
    public TextMeshProUGUI upgradeCost;
    public TextMeshProUGUI sellCost;
    public Button upgradeButton;
    
    private Node target;

    public void SetTarget(Node _target)
    {
        target = _target;

        transform.position = target.GetBuildPosition();
        
        if (!SuperMode)
        {
            upgradeCost.text = "-";
            upgradeButton.interactable = false;
        }
        
        if (target.isUpgraded)
        {
            sellCost.text = target.turretBP.GetSellAmount2().ToString() + "$";
            upgradeCost.text = "DONE";
            upgradeButton.interactable = false;
        }
        
        else if (!target.isUpgraded)
        {
            sellCost.text = target.turretBP.GetSellAmount().ToString() + "$";
            upgradeCost.text = target.turretBP.upgradeCost.ToString() + "$";
            upgradeButton.interactable = true;
        }
        
        ui.SetActive(true);
    }
    
    public void Update()
    { 
        if (!SuperMode)
        {
            upgradeCost.text = "-";
            upgradeButton.interactable = false;
        }
    }

    public void Hide()
    {
        ui.SetActive(false);
    }
    
    public void Upgrade()
    {
        target.UpgradeTurret();
        BuildManager.instance.DeSelectNode();
        var audiosource = GetComponent<AudioSource>();
        audiosource.Play();
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeSelectNode();
        var audiosource = GetComponent<AudioSource>();
        audiosource.Play();
    }
}
