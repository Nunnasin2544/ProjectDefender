using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool gameEnded = false;

    void Update () 
    {
        if (gameEnded)
        {
            SceneManager.LoadScene("GameOver");
            return;
        } 
    
        if (PlayerStats.Lives <= 0)
        {
            PlayerStats.Lives = 0;
            EndGame();
        }
    }
    
    void EndGame()
    {
        gameEnded = true;
        Debug.Log("Game Over!");
    }
}
