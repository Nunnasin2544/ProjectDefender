using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurretText : MonoBehaviour
{
    public TextMeshProUGUI turretText;
    
        void Update()
        {
            turretText.text = BuildManager.CurrentTurret.ToString() + "/" + BuildManager.LimitTurret.ToString();
        }
}
