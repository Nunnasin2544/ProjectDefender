using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WaveText : MonoBehaviour
{
    public TextMeshProUGUI waveText;
    
        void Update()
        {
            waveText.text = "Wave " + WaveSpawner.waveCount.ToString();
        }
}
