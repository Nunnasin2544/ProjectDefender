using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PhaseText : MonoBehaviour
{
    public TextMeshProUGUI phaseText;
    
        void Update()
        {
            if (WaveSpawner.EnemiesAlive > 0 || !WaveSpawner.endAllWave|| WaveSpawner.endSpawner)
            phaseText.text = "Battle";
            
            else if (WaveSpawner.EnemiesAlive == 0 && WaveSpawner.endAllWave)
            phaseText.text = "Build";
        }
}
